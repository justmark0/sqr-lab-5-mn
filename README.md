# Lab5 -- Integration testing

### Homework

## BVA

| Parameter        | Possible classes              |
|------------------|-------------------------------|
| type             | budget, luxury, nonsense      |
| plan             | minute, fixed_price, nonsense |
| distance         | <=0, >0, >=1000001            |
| planned_distance | <=0, >0, >=1000001            |
| time             | <=0, >0, >=1000001            |
| planned_time     | <=0, >0, >=1000001            |
| inno_discount    | yes, no, nonsense             |

### Decision table

Here is InnoCar Specs:
Budet car price per minute = 22
Luxury car price per minute = 59
Fixed price per km = 16
Allowed deviations in % = 14.000000000000002
Inno discount in % = 5


| type     | plan        | distance        | planned_distance | time            | planned_time    | discount | status  |
|----------|-------------|-----------------|------------------|-----------------|-----------------|----------|---------|
| budget   | minute      | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | yes      | Valid   |
| budget   | minute      | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | no       | Valid   |
| budget   | fixed_price | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | yes      | Valid   |
| budget   | fixed_price | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | no       | Valid   |
| luxury   | minute      | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | yes      | Valid   |
| luxury   | minute      | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | no       | Valid   |
| luxury   | fixed_price | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | yes      | Valid   |
| luxury   | fixed_price | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | no       | Valid   |
| nonsense | *           | *               | *                | *               | *               | *        | Invalid |
| *        | nonsense    | *               | *                | *               | *               | *        | Invalid |
| *        | *           | <=0             | *                | *               | *               | *        | Invalid |
| *        | *           | >1000000        | *                | *               | *               | *        | Invalid |
| *        | *           | *               | <=0              | *               | *               | *        | Invalid |
| *        | *           | *               | >1000000         | *               | *               | *        | Invalid |
| *        | *           | *               | *                | <=0             | *               | *        | Invalid |
| *        | *           | *               | *                | >1000000        | *               | *        | Invalid |
| *        | *           | *               | *                | *               | <=0             | *        | Invalid |
| *        | *           | *               | *                | *               | >1000000        | *        | Invalid |
| *        | *           | *               | *                | *               | *               | nonsense | Invalid |


### Results
Tests for decision table

| type    | plan           | distance  | planned_distance | time           | planned_time   | inno_discount | expected        | actual          | equal  |
|---------|----------------|-----------|------------------|----------------|----------------|---------------|-----------------|-----------------|--------|
| budget  | minute         | 100       | 150              | 200            | 250            | yes           | 4180.0          | 4180            | Yes    |
| budget  | minute         | 100       | 150              | 200            | 250            | no            | 4400            | 4400            | Yes    |
| budget  | fixed_price    | 100       | 150              | 200            | 250            | yes           | 4180.0          | 3166.6666666666 | No     |
| budget  | fixed_price    | 100       | 150              | 200            | 250            | no            | 4400            | 3333.3333333333 | No     |
| luxury  | minute         | 100       | 150              | 200            | 250            | yes           | 11210.0         | 11210           | Yes    |
| luxury  | minute         | 100       | 150              | 200            | 250            | no            | 11800           | 11800           | Yes    |
| luxury  | fixed_price    | 100       | 150              | 200            | 250            | yes           | 11210.0         | Invalid Request | No     |
| luxury  | fixed_price    | 100       | 150              | 200            | 250            | no            | 11800           | Invalid Request | No     |
| nonsense| minute         | 1         | 1                | 1              | 1              | yes           | Invalid Request | Invalid Request | Yes    |
| budget  | nonsense       | 1         | 1                | 1              | 1              | yes           | Invalid Request | Invalid Request | Yes    |
| budget  | minute         | 0         | 1                | 1              | 1              | yes           | Invalid Request | 20.9            | No     |
| budget  | minute         | 1000001   | 1                | 1              | 1              | yes           | Invalid Request | 20.9            | Yes    |
| budget  | minute         | 1         | 0                | 1              | 1              | yes           | Invalid Request | 20.9            | No     |
| budget  | minute         | 1         | 1000001          | 1              | 1              | yes           | Invalid Request | 20.9            | Yes    |
| budget  | minute         | 1         | 1                | 0              | 1              | yes           | Invalid Request | 0               | No     |
| budget  | minute         | 1         | 1                | 1000001        | 1              | yes           | Invalid Request | 20900020.9      | Yes    |
| budget  | minute         | 1         | 1                | 1              | 0              | yes           | Invalid Request | 20.9            | No     |
| budget  | minute         | 1         | 1                | 1              | 1000001        | yes           | Invalid Request | 20.9            | Yes    |
| budget  | minute         | 1         | 1                | 1              | 1              | nonsense      | Invalid Request | Invalid Request | Yes    |


### Bugs

1. Requests are wrongly validated
2. Price calculation is wrong for luxury type and fixed price plan

